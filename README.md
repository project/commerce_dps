# Commerce DPS

[Commerce DPS](http://drupal.org/project/commerce_dps) integrates DPS
(aka PaymentExpress) with Drupal Commerce payment and checkout system.

## Features

Commerce DPS supports both merchant and gateway hosted billing methods, and
token billing ([in progress](https://drupal.org/node/2397663)).

## Available Payment Methods

### PX Pay

[PX Pay](https://www.paymentexpress.com/Products/Ecommerce/Payment_Express_Hosted)
is a payment solution hosted by Payment Express; your site won't see credit
card details so you have less requirements for compliance. It's safer and
simpler to implement.

### PX Post

[PX Post](https://www.paymentexpress.com/Technical_Resources/Ecommerce_NonHosted/PxPost)
is a payment solution you host on your site. Credit cards are entered at
checkout on your site. You need to check with your bank whether they permit
merchant-hosted checkout.

*PX Post is not yet implemented for Drupal 8 / 9*. To support implementation,
[see #3205977](https://www.drupal.org/project/commerce_dps/issues/3205977) or
contact the module maintainer to see how to make it happen! Contribution is
welcome.

## Installation

1. Install using composer: `composer require drupal/commerce_dps:^2.0`
2. Enable Commerce DPS: `drush en commerce_dps` (or use the web UI)
3. Add a new payment gateway at `/admin/commerce/config/payment-gateways`
4. Configure the gateway with your DPS credentials.

Pretty much all the details required in the configuration interface are
obtained from Payment Express. If the inline documentation isn't clear, please
[file an issue](https://www.drupal.org/project/issues/commerce_dps) and help
improve the interface! See below for instructions.

## Troubleshooting

For any questions, the first point of reference should be the issue queue on
Drupal.org. Please use the search interface provided to see if your problem has
been fixed or is currently being worked on, and if not create a new issue to
discuss it.

Always check your site error logs (both webserver and Drupal logs) and give
detail when making a report.

Documentation fixes are welcome!

## Contributing

It's preferred that you use the Drupal.org issue queue, but contributions are
welcome via [Github](https://github.com/xurizaemon/commerce_dps) also.

## Credits

* The Commerce DPS maintainer is [Chris Burgess](https://drupal.org/user/76026)
* [Committers to Commerce DPS](https://www.drupal.org/node/1496210/committers)
* Windcave team, for providing a solid and professional merchant service
* Thanks also to everyone who's contributed by reporting issues, making
  suggestions, reviewing code or supporting work.
